﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="Marikina.Events" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    
     <div class="image"><img class="undefined" runat="server" src="~/assets/img/eventsbanner.png" style="width:100%; height:450px; padding-bottom:10px;"></div>
           
       
    
     <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner" style="padding: 20px;">
                            <div class="portfolio-content portfolio-1">
                               
                                <div id="js-grid-juicy-projects" class="cbp">
                                    <div class="cbp-item graphic">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/013.jpg"  class="thumbevent" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                        <a href="../assets/global/img/portfolio/1200x900/57.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">Event1</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">June 18, 2015</div>
                                    </div>
                                    <div class="cbp-item web-design logos">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/05.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                       <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                        <a href="../assets/global/img/portfolio/1200x900/50.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center">Event2</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">July 12, 2015</div>
                                    </div>
                                    <div class="cbp-item graphic logos">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/16.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                       <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                         <a href="../assets/global/img/portfolio/600x600/16.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center">Event3</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">August 1, 2015</div>
                                    </div>
                                    <div class="cbp-item web-design graphic">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/33.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                       <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                        <a href="../assets/global/img/portfolio/600x600/33.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center">Event4</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">September 8, 2015</div>
                                    </div>
                                    <div class="cbp-item identity web-design">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/38.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                        <a href="../assets/global/img/portfolio/1200x900/4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="WhereTO App<br>by Tiberiu Neamu">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center">Event5</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">October 25, 2015</div>
                                    </div>
                                    <div class="cbp-item identity web-design">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/88.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                        <a href="../assets/global/img/portfolio/1200x900/7.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Ski * Buddy<br>by Tiberiu Neamu">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center">Event6</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">November 5, 2015</div>
                                    </div>
                                    <div class="cbp-item graphic logos">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/02.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                       <a href="indivEvent.aspx" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                                         <a href="../assets/global/img/portfolio/600x600/02.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">view larger</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">Event6</div>
                                        <div class="cbp-l-grid-projects-desc uppercase text-center">December 1, 2015</div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->            
    
           
</asp:Content>
